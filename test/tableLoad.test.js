const Sequelize = require("sequelize");
const assert = require('assert');
const {connect, get} = require('../src/index');
const path = require('path');

describe('sequelize-dao', function () {

    describe('load', function () {

        before(async function () {
            await connect({
                dialect: "sqlite",
                storage: "sqlite.db",
                order: [
                    path.resolve(__dirname, "models/Users.model.js"),
                    path.resolve(__dirname, "models/Properties.model.js")
                ]
            })
        });

        it('select user data', async function () {
            let expectedUser = [
                {
                    username: 'FT',
                    password: 'Pippo'
                },
                {
                    username: 'tetofonta',
                    password: 'Baudo'
                }
            ];

            let usersData = await get('Users').findAll();
            assert.ok(usersData.length === 2, "Not received all data");
            usersData.forEach((e, i) => {
                assert.equal(e.username, expectedUser[i].username);
                assert.equal(e.password, expectedUser[i].password);
            })
        });

        it('props data', async function () {
            let expectedProperties = [
                {
                    username: 'FT',
                    nome: 'Francesco',
                    cognome: 'Torli'
                },
                {
                    username: 'tetofonta',
                    nome: 'Stefano',
                    cognome: 'Fontana'
                }
            ];

            let propertiesData = await get('Properties').findAll();
            assert.ok(propertiesData.length === 2, "Not received all data");
            propertiesData.forEach((e, i) => {
                assert.equal(e.username, expectedProperties[i].username);
                assert.equal(e.nome, expectedProperties[i].nome);
                assert.equal(e.cognome, expectedProperties[i].cognome);
            })
        });
    });
});