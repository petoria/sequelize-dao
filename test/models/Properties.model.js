const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
            references: {
                model: 'Users',
                key: 'username'
            }
        },
        nome: {
            type: Sequelize.STRING
        },
        cognome: {
            type: Sequelize.STRING
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            username: 'FT',
            nome: 'Francesco',
            cognome: 'Torli'
        },
        {
            username: 'tetofonta',
            nome: 'Stefano',
            cognome: 'Fontana'
        }
    ]
};