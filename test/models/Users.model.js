const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        username: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
        field: {
            type: Sequelize.STRING
        }
    };
};

module.exports.registered = function (table) {
    table.beforeCreate((user, options) => {
        user.field = 'ciao';
    });
};

module.exports.default_data = function () {
    return [
        {
            username: 'FT',
            password: 'Pippo'
        },
        {
            username: 'tetofonta',
            password: 'Baudo'
        }
    ]
};