const Sequelize = require("sequelize");
const fs = require("fs");
const {promisify} = require("util");

const readdir = promisify(fs.readdir);

async function connect(CProps) {
    if (CProps.database)
        global.sequelize = new Sequelize(
            CProps.database,
            CProps.username,
            CProps.password,
            {
                host: CProps.hostname,
                dialect: CProps.dialect,
                pool: CProps.pool
            }
        );
    else
        global.sequelize = new Sequelize(CProps);

    sequelize.tables = {};
    await sequelize.authenticate();
    let tables = CProps.order;
    for (let table of tables) {
        const t = require(table);
        const tableName = table.substr(table.lastIndexOf('/') + 1, table.indexOf('.', 0) - (table.lastIndexOf('/') + 1));

        const o = t.register();
        sequelize.tables[tableName] = await sequelize.define(tableName, o);

        Object.keys(o).forEach(e => {
            const o = t.register()[e];
            if (o.references) {
                sequelize.tables[o.references.model].hasMany(sequelize.tables[tableName], {foreignKey: o.references.key});
                sequelize.tables[tableName].belongsTo(sequelize.tables[o.references.model], {foreignKey: e});
                console.log(`Created relationship ${o.references.model}.${o.references.key} n<=>1 ${tableName}.${e}`)
            }
        });
        try {
            await sequelize.tables[tableName].findAll({attributes: Object.keys(o)})
        } catch (e) {
            console.log("RECREATING " + tableName + e);
            await sequelize.tables[tableName].drop();
            await sequelize.tables[tableName].sync();

            let data = t.default_data();
            await Promise.all(data.map(async (e) => {
                await sequelize.tables[tableName].build(e).save();
            }))
        }
        t.registered(sequelize.tables[tableName]);
    }

    return sequelize;
}

function get(table) {
    return sequelize.tables[table]
}

module.exports = {connect, get};